import express, { Express } from "express";
import cors from "cors";

import authRoute from "./src/routes/auth.route";
import authorRoute from "./src/routes/author.route";
import categoriesRoute from "./src/routes/categories.route";
import productRoute from "./src/routes/products.route";
import addressRoute from "./src/routes/address.route";
import cartRoute from "./src/routes/cart.route";
import shippingRoute from "./src/routes/shipping.route";
import transactionRoute from "./src/routes/transaction.route";
import paymentRoute from "./src/routes/payment.route";
import reviewRoute from "./src/routes/review.route";

import dotenv from "dotenv";
import { connect } from "./src/utils/rabbitmq.utils";
dotenv.config();

const app: Express = express();
const port = process.env.PORT || 3000;

/** Middlewares */
app.use(cors());
app.use(express.json());

const api = express.Router();
app.use("/api/v1", api);

api.use("/auth", authRoute);
api.use("/categories", categoriesRoute);
api.use("/products", productRoute);
api.use("/authors", authorRoute);
api.use("/address", addressRoute);
api.use("/cart", cartRoute);
api.use("/shipping", shippingRoute);
api.use("/transaction", transactionRoute);
api.use("/payment", paymentRoute);
api.use("/review", reviewRoute);

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
