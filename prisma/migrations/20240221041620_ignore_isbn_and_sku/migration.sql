-- AlterTable
ALTER TABLE `Products` MODIFY `isbn` VARCHAR(191) NULL,
    MODIFY `sku` VARCHAR(191) NULL;
