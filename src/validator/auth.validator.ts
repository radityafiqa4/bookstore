import Joi from "joi";

export const register = Joi.object({
  name: Joi.string().min(3).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required(),
});

export const login = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required(),
});

export const verifyPhone = Joi.object({
  phone: Joi.string()
    .pattern(/^(62)\d{10,13}$/)
    .required()
    .messages({
      "string.pattern.base": "Phone number not valid, must start with 62",
      "string.empty": "Phone number is required",
    }),
});

export const verifyPhoneCode = Joi.object({
  code: Joi.string().length(6).required(),
});
