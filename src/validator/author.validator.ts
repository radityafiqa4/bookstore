import Joi from "joi";

export const author = Joi.object({
    name: Joi.string().required(),
});
