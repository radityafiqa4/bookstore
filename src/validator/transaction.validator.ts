import Joi from "joi";

export const transaction = Joi.object({
  addressId: Joi.string().uuid().required(),
  paymentId: Joi.number().required(),
  courierId: Joi.string().uuid().required(),
  courierService: Joi.string().required(),
});
