import Joi from "joi";

export const categories = Joi.object({
  name: Joi.string().min(3).required(),
});
