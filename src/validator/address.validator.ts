import Joi from "joi";

export const address = Joi.object({
  name: Joi.string().required(),
  street: Joi.string().required(),
  provinceId: Joi.number().required(),
  districtId: Joi.number().required(),
  subdistrictId: Joi.number().required(),
  postalCode: Joi.number().required(),
  phone: Joi.string()
    .pattern(/^(62)\d{10,13}$/)
    .required()
    .messages({
      "string.pattern.base": "Phone number not valid, must start with 62",
      "string.empty": "Phone number is required",
    }),
});
