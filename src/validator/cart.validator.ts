import Joi from "joi";

export const cart = Joi.object({
  productId: Joi.string().required(),
  quantity: Joi.number().required(),
});
