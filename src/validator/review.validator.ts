import Joi from "joi";

export const review = Joi.object({
  rating: Joi.number().min(1).max(5).required(),
  review: Joi.string().required(),
});
