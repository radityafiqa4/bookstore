import Joi from "joi";

export const createProduct = Joi.object({
  name: Joi.string().min(3),
  price: Joi.number().max(100000000),
  stock: Joi.number().max(100000000),
  publishedAt: Joi.date(),
  publisher: Joi.string().min(3),
  description: Joi.string().min(3),
  thumbnail: Joi.number().min(0).max(5),
  width: Joi.number(),
  height: Joi.number(),
  pages: Joi.number(),
  language: Joi.string().min(3),
  isbn: Joi.string().min(3),
  weight: Joi.number(),
  sku: Joi.string().min(3),
  categories: Joi.array().items(Joi.string()),
  authors: Joi.array().items(Joi.string()),
});
