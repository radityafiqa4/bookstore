import { Router } from "express";
import {
  getAdminTransactions,
  getTransactionById,
  getUserTransactions,
  placeOrder,
} from "../controllers/transactions.controllers";
import isUser from "../middleware/isUser.middleware";
import isAdmin from "../middleware/isAdmin.middleware";
import validate from "../middleware/validator.middleware";
import { transaction } from "../validator/transaction.validator";
const router = Router();

router.get("/admin", isAdmin, getAdminTransactions);
router.post("/", isUser, validate(transaction), placeOrder);
router.get("/", isUser, getUserTransactions);
router.get("/:id", isUser, getTransactionById);
router.post("/:id/shipping", isAdmin, getTransactionById);
router.post("/:id/shipping/callback", getTransactionById);

export default router;
