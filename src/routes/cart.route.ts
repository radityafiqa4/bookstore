import { Router } from "express";
import { addToCart, editCart, getCart } from "../controllers/cart.controllers";
import { cart } from "../validator/cart.validator";
import validate from "../middleware/validator.middleware";
import isUser from "../middleware/isUser.middleware";
const router = Router();

router.get("/", isUser, getCart);
router.post("/", isUser, validate(cart), addToCart);
router.put("/", isUser, validate(cart), editCart);

export default router;
