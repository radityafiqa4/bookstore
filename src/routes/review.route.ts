import { Router } from "express";
import {
  createReview,
  getReviewsByTransactionId,
} from "../controllers/reviews.controllers";
import isUser from "../middleware/isUser.middleware";
import validate from "../middleware/validator.middleware";
import { review } from "../validator/review.validator";

const router = Router();

router.get("/:transactionId", isUser, getReviewsByTransactionId);

router.post(
  "/:transactionId/:productId",
  isUser,
  validate(review),
  createReview
);

export default router;
