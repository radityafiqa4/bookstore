import { Router } from "express";
import {
  getProduct,
  getProducts,
  createProduct,
  updateProduct,
  deleteProduct,
  addImageProduct,
  deleteImageProduct,
} from "../controllers/products.controllers";

import { createProduct as createSchema } from "../validator/products.validator";

import upload from "../middleware/uploadPhoto.middleware";
import validate from "../middleware/validator.middleware";
import isAdmin from "../middleware/isAdmin.middleware";

const router = Router();

router.get("/", getProducts);
router.get("/:slug", getProduct);
router.post("/", isAdmin, upload, validate(createSchema), createProduct);
router.put("/:id", isAdmin, validate(createSchema), updateProduct);
router.delete("/:id", isAdmin, deleteProduct);
router.post("/:id/images", isAdmin, upload, addImageProduct);
router.delete("/:id/images/:imageId", isAdmin, deleteImageProduct);

export default router;
