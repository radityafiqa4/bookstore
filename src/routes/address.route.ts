import { Router } from "express";
import validate from "../middleware/validator.middleware";
import { address as addressSchema } from "../validator/address.validator";
import {
  createUserAddress,
  getUserAddresses,
  updateUserAddress,
  deleteUserAddress,
  getAddress,
} from "../controllers/address.controllers";
import isUser from "../middleware/isUser.middleware";

const router = Router();

router.get("/", getAddress);
router.post("/user", isUser, validate(addressSchema), createUserAddress);
router.get("/user", isUser, getUserAddresses);
router.put("/user/:id", isUser, validate(addressSchema), updateUserAddress);
router.delete("/user/:id", isUser, deleteUserAddress);

export default router;
