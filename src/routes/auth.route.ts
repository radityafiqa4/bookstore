import { Router } from "express";
import validate from "../middleware/validator.middleware";
import {
  register as RegisterSchema,
  login as LoginSchema,
  verifyPhone as VerifyPhoneSchema,
  verifyPhoneCode as verifyPhoneCodeSchema,
} from "../validator/auth.validator";
import {
  login,
  register,
  verifyPhone,
  verifyPhoneCode,
} from "../controllers/auth.controllers";
import isUser from "../middleware/isUser.middleware";
const router = Router();

router.post("/login", validate(LoginSchema), login);
router.post("/register", validate(RegisterSchema), register);
router.post("/verify", isUser, validate(VerifyPhoneSchema), verifyPhone);
router.post(
  "/verify-code",
  isUser,
  validate(verifyPhoneCodeSchema),
  verifyPhoneCode
);

export default router;
