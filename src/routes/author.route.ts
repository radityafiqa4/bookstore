import { Router } from "express";
import validate from "../middleware/validator.middleware";
import { author as authorSchema } from "../validator/author.validator";
import {
  getAuthor,
  getAuthors,
  createAuthor,
  updateAuthor,
  deleteAuthor,
} from "../controllers/author.controllers";
import isAdmin from "../middleware/isAdmin.middleware";

const router = Router();

router.get("/", getAuthors);
router.get("/:id", getAuthor);
router.post("/", isAdmin, validate(authorSchema), createAuthor);
router.put("/:id", isAdmin, validate(authorSchema), updateAuthor);
router.delete("/:id", isAdmin, deleteAuthor);

export default router;
