import { Router } from "express";
import {
  getCouriers,
  getShippingCost,
} from "../controllers/shipping.controllers";
import isUser from "../middleware/isUser.middleware";

const router = Router();

router.get("/couriers", getCouriers);
router.get("/cost", isUser, getShippingCost);

export default router;
