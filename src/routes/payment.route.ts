import { Router } from "express";
import { paymentCallback } from "../controllers/payment.controllers";

const router = Router();

router.post("/callback", paymentCallback);

export default router;
