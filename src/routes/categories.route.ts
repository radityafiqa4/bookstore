import { Router } from "express";
import validate from "../middleware/validator.middleware";
import { categories as categoriesSchema } from "../validator/categories.validator";
import {
  getCategory,
  getCategories,
  createCategory,
  updateCategory,
  deleteCategory,
} from "../controllers/category.controllers";
import isAdmin from "../middleware/isAdmin.middleware";
const router = Router();

router.get("/", getCategories);
router.get("/:slug", getCategory);
router.post("/", isAdmin, validate(categoriesSchema), createCategory);
router.put("/:id", isAdmin, validate(categoriesSchema), updateCategory);
router.delete("/:id", isAdmin, deleteCategory);

export default router;
