import amqp from "amqplib";
import dotenv from "dotenv";
dotenv.config();

export const connect = async () => {
  const connection = await amqp.connect(
    process.env.RABBITMQ_URL || "amqp://localhost"
  );

  const channel = await connection.createChannel();
  return { connection, channel };
};
