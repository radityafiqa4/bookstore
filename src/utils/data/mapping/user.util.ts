const UserMapper = (user: any) => {
  delete user.password;
  delete user.code;

  return {
    ...user,
  };
};

export default UserMapper;
