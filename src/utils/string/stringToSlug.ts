const stringToSlug = (str: string) => {
  str = str.replace(/^\s+|\s+$/g, ""); // trim
  str = str.toLowerCase(); // lowercase
  str = str.replace(/[^a-z0-9 -]/g, ""); // remove invalid chars
  str = str.replace(/\s+/g, "-"); // collapse whitespace and replace by -
  str = str.replace(/-+/g, "-"); // collapse dashes

  return str;
};

export default stringToSlug;
