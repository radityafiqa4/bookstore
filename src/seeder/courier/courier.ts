import prisma from "../../database/prisma.database";
import data from "./data.json";

async function main() {
  await prisma.courier.deleteMany();

  await prisma.courier.createMany({
    data: data.map((courier: any) => ({
      name: courier.name,
      key: courier.key,
      image: courier.image,
    })),
  });
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
