import prisma from "../../database/prisma.database";
import data from "./data.json";

async function main() {
  await prisma.categories.deleteMany({});
  await prisma.categories.createMany({
    data: data,
  });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
