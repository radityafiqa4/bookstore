import prisma from "../../database/prisma.database";
import data from "./data.json";

interface City {
  id: string;
  name: string;
  districts: District[];
}

interface District {
  id_district: string;
  name: string;
  city_type: string;
  city: string;
}

interface Province {
  [provinceName: string]: {
    id: string;
    name: string;
    cities: City[];
  };
}

async function main() {
  await prisma.province.deleteMany();

  const provinces: Province = data;
  const counter = 1;

  for (const province in provinces) {
    const provinceData = provinces[province];
    const provinceRecord = await prisma.province.create({
      data: {
        id: parseInt(provinceData.id),
        name: provinceData.name,
      },
    });

    console.log(
      `Seeded [${counter}/${Object.keys(provinces).length}] ${
        provinceData.name
      }`
    );

    for (const city of provinceData.cities) {
      const cityRecord = await prisma.district.create({
        data: {
          id: parseInt(city.id),
          name: city.name,
          provinceId: provinceRecord.id,
        },
      });

      for (const district of city.districts) {
        await prisma.subdistrict.create({
          data: {
            id: parseInt(district.id_district),
            name: district.name,
            districtId: cityRecord.id,
          },
        });
      }
    }
  }
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
