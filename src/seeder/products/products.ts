import prisma from "../../database/prisma.database";
import data from "./data.json";

interface Book {
  name: string;
  slug: string;
  categories: string[];
  price: number;
  publishDate: string;
  authors: string[];
  publisher: string;
  description: string;
  thumbnail: string;
  photo: string[];
  width: string;
  height: string;
  pages: string;
  language: string;
  isbn: string;
  weight: string;
  sku: string;
}

async function seedBooks(books: Book[]) {
  console.log(`Seeding ${books.length} books...`);

  for (const book of books) {
    const { categories, authors, photo, ...rest } = book;

    // Find or create categories
    const categoriesPromises = categories.map(async (category) => {
      return prisma.categories.upsert({
        where: { slug: category },
        update: {},
        create: { name: category, slug: category },
      });
    });

    const createdCategories = await Promise.all(categoriesPromises);

    const authorsPromises = authors.map(async (author) => {
      return prisma.author.upsert({
        where: { name: author },
        update: {},
        create: { name: author },
      });
    });

    const createdAuthors = await Promise.all(authorsPromises);

    const connectCategories = createdCategories.map((c) => ({ id: c.id }));
    const connectAuthors = createdAuthors.map((a) => ({ id: a.id }));

    // Create product
    const product = await prisma.products.create({
      data: {
        name: rest.name,
        slug: rest.slug,
        price: rest.price,
        publishedAt: new Date(rest.publishDate),
        publisher: rest.publisher,
        description: rest.description,
        thumbnail: rest.thumbnail,
        width: parseInt(rest.width),
        height: parseInt(rest.height),
        pages: parseInt(rest.pages),
        language: rest.language,
        isbn: rest.isbn,
        weight: parseFloat(rest.weight),
        sku: rest.sku,
        stock: 100,
        images: { create: photo.map((url) => ({ url })) },
      },
    });

    await prisma.productsCategories.createMany({
      data: connectCategories.map((c) => ({
        productId: product.id,
        categoryId: c.id,
      })),
    });

    await prisma.authorsProducts.createMany({
      data: connectAuthors.map((a) => ({
        productId: product.id,
        authorId: a.id,
      })),
    });

    console.log(`Successfully seeded book: ${rest.name}`);
  }
}

async function main() {
  await prisma.products.deleteMany({}); // Clear existing data
  const books: Book[] = data as any; // Assuming `data` is your book data
  await seedBooks(books);
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
