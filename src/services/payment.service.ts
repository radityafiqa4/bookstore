import fetch from "node-fetch";

export const getPaymentChannel = async () => {
  try {
    const response = await fetch(
      `${process.env.PAYMENT_GATEWAY_URL}/payment/channel`
    ).then((res) => res.json());

    if (response.status == "failed") {
      throw new Error(response.message);
    }

    return response.data;
  } catch (error: any) {
    throw new Error(error.message);
  }
};

export const createPayment = async (data: any) => {
  try {
    const response = await fetch(`${process.env.PAYMENT_GATEWAY_URL}/payment`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    }).then((res) => res.json());

    if (response.status !== "success") {
      throw new Error(response.message);
    }

    return response.data;
  } catch (error: any) {
    throw new Error(error.message);
  }
};
