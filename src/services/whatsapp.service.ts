import fetch from "node-fetch";
import dotenv from "dotenv";
dotenv.config();

export const sendWhatsappMessage = async (phone: string, message: string) => {
  try {
    const response = await fetch(
      `${process.env.WHATSAPP_GATEWAY_URL}/kirimPesan`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          token: process.env.WHATSAPP_GATEWAY_TOKEN,
          target: phone,
          message: message,
          type: "text",
          delay: "1",
        }),
      }
    );

    const data = await response.json();
    return data;
  } catch (error: any) {
    throw new Error(error.message);
  }
};
