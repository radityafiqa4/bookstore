import prisma from "../database/prisma.database";

const getWeightCart = (products: any[]) => {
  const weight = products.reduce(
    (acc, curr) => acc + curr.product.weight * curr.quantity,
    0
  );

  return weight;
};

const getTotalCart = (products: any[]) => {
  const total = products.reduce(
    (acc, curr) => acc + curr.product.price * curr.quantity,
    0
  );

  return total;
};

const getCartByUserId = async (userId: string) => {
  const cart = await prisma.cart.findMany({
    where: {
      userId: userId,
    },
    include: {
      product: true,
    },
  });

  return cart;
};

export { getWeightCart, getCartByUserId, getTotalCart };
