import prisma from "../database/prisma.database";

export const getProductTotalSold = async (productsId: string[]) => {
  return await prisma.transactionDetail.groupBy({
    by: ["productId"],
    _count: {
      id: true,
    },
    where: {
      productId: {
        in: productsId,
      },
    },
  });
};

export const getProductReviewAvg = async (productsId: string[]) => {
  return await prisma.review.groupBy({
    by: ["productId"],
    _avg: {
      rating: true,
    },
    where: {
      productId: {
        in: productsId,
      },
    },
  });
};
