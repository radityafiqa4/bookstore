import prisma from "../database/prisma.database";

export const getAddressByAddressUserId = async (
  userId: string,
  addressId: string
) => {
  const addresses = await prisma.address.findFirst({
    where: {
      id: addressId,
      userId: userId,
    },
  });

  return addresses;
};
