import fetch from "node-fetch";
import prisma from "../database/prisma.database";

export const getCourierCost = async (
  courier: string,
  destination: number,
  weight: number
) => {
  try {
    const query = new URLSearchParams({
      origin: "364",
      originType: "subdistrict",
      destination: destination.toString(),
      destinationType: "subdistrict",
      berat: weight.toString(),
      kurir: courier,
    });

    const response = await fetch(
      `http://api.xfatih.com/checker/cek_ongkir.php?${query}`
    ).then((res) => res.json());

    return response["SayaReja"]["results"][0]["costs"];
  } catch (error: any) {
    throw new Error(error.message);
  }
};

export const getAllCouriers = async () => {
  const courier = await prisma.courier.findMany();
  return courier;
};

export const getCourierServiceByResponse = (
  response: any,
  courierService: string
) => {
  return response.find((cost: any) => cost.service === courierService).cost[0]
    .value;
};

export const getCouriersByCourierId = async (courierId: string) => {
  const courier = await prisma.courier.findUnique({
    where: {
      id: courierId,
    },
  });
  return courier;
};
