import { Request, Response } from "express";
import response from "../utils/response.util";
import prisma from "../database/prisma.database";

export const createAuthor = async (req: Request, res: Response) => {
  let { name } = req.body;

  try {
    const Author = await prisma.author.create({
      data: {
        name,
      },
    });

    return response.success(res, "Author created", Author);
  } catch (error: any) {
    return response.failed(res, "Failed to create Author", 500);
  }
};

export const getAuthors = async (req: Request, res: Response) => {
  try {
    let { page, limit, query } = req.query;

    const pageNumber = Math.max(1, parseInt(page as string) || 1);
    const limitNumber = Math.max(
      1,
      Math.min(50, parseInt(limit as string) || 20)
    );

    const queryFilter = query ? { name: { contains: query as string } } : {};

    const filter = {
      where: {
        ...queryFilter,
      },
    };

    const authors = await prisma.author.findMany({
      skip: (pageNumber - 1) * limitNumber,
      take: limitNumber,
      ...filter,
    });

    const totalData = await prisma.author.count(filter);

    return response.success(res, "Success get Author", authors, {
      current_page: pageNumber,
      limit: limitNumber,
      total_data: totalData,
      total_page: Math.ceil(totalData / limitNumber),
    });

  } catch (error: any) {
    return response.failed(res, "Failed to get Author", 500);
  }
};

export const getAuthor = async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const author = await prisma.author.findUnique({
      where: { id },
    });

    if (!author) {
      return response.failed(res, "Author not found", 404);
    }

    return response.success(res, "Success get Author", author);
  } catch (error: any) {
    return response.failed(res, "Failed to get Author", 500);
  }
};

export const updateAuthor = async (req: Request, res: Response) => {
  let { id } = req.params;
  let { name } = req.body;

  try {
    const author = await prisma.author.update({
      where: { id: id },
      data: {
        name,
      },
    });

    return response.success(res, "Author updated", author);
  } catch (error: any) {
    return response.failed(res, "Failed to update Author", 500);
  }
};

export const deleteAuthor = async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const author = await prisma.author.delete({
      where: { id },
    });

    return response.success(res, "Author deleted", author);
  } catch (error: any) {
    return response.failed(res, "Failed to delete Author", 500);
  }
};
