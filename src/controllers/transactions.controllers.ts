import { Request, Response } from "express";
import prisma from "../database/prisma.database";
import response from "../utils/response.util";
import { getAddressByAddressUserId } from "../services/address.service";
import {
  getCourierCost,
  getCourierServiceByResponse,
  getCouriersByCourierId,
} from "../services/shipping.service";
import {
  getCartByUserId,
  getTotalCart,
  getWeightCart,
} from "../services/cart.service";
import { whatsappNotificationProducer } from "../producers/whatsapp.producers";
import { createPayment, getPaymentChannel } from "../services/payment.service";

export const getAdminTransactions = async (req: Request, res: Response) => {
  let { page, limit } = req.query;

  try {
    const pageNumber = Math.max(1, parseInt(page as string) || 1);
    const limitNumber = Math.max(
      1,
      Math.min(50, parseInt(limit as string) || 20)
    );

    const transactions = await prisma.transaction.findMany({
      take: limitNumber,
      skip: (pageNumber - 1) * limitNumber,
      orderBy: {
        createdAt: "desc",
      },
      include: {
        user: true,
        payment: true,
        address: true,
        courier: true,
        details: {
          include: {
            product: true,
          },
        },
      },
    });

    const totalData = await prisma.transaction.count();

    return response.success(
      res,
      "Transactions retrieved successfully",
      transactions,
      {
        current_page: pageNumber,
        limit: limitNumber,
        total_data: totalData,
        total_page: Math.ceil(totalData / limitNumber),
      }
    );
  } catch (error: any) {
    return response.failed(res, "Error retrieving transactions", 500);
  }
};

export const getUserTransactions = async (req: Request, res: Response) => {
  try {
    const transactions = await prisma.transaction.findMany({
      where: {
        userId: req.userId,
      },
      orderBy: {
        createdAt: "desc",
      },
    });

    return response.success(
      res,
      "Transactions retrieved successfully",
      transactions
    );
  } catch (error: any) {
    return response.failed(res, "Error retrieving transactions", 500);
  }
};

export const getTransactionById = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const transaction = await prisma.transaction.findUnique({
      where: {
        id: String(id),
        userId: req.userId,
      },
      include: {
        payment: true,
        address: true,
        courier: true,
        details: {
          include: {
            product: true,
          },
        },
      },
    });

    if (!transaction) {
      return response.failed(res, "Transaction not found", 404);
    }

    return response.success(
      res,
      "Transaction retrieved successfully",
      transaction
    );
  } catch (error: any) {
    return response.failed(res, "Error retrieving transaction", 500);
  }
};

export const placeOrder = async (req: Request, res: Response) => {
  try {
    const { addressId, courierId, paymentId, courierService } = req.body;

    const [cart, courier, address, payment] = await Promise.all([
      getCartByUserId(req.userId),
      getCouriersByCourierId(courierId),
      getAddressByAddressUserId(req.userId, addressId),
      getPaymentChannel(),
    ]);

    if (
      !cart ||
      (cart && cart.length === 0) ||
      !courier ||
      !address ||
      !payment
    ) {
      return response.failed(res, "Invalid request", 400);
    }

    const selectedPayment = payment.find(
      (item: any) => parseInt(item.id) === parseInt(paymentId)
    );

    if (
      !selectedPayment ||
      (selectedPayment && selectedPayment.status !== "ON")
    ) {
      return response.failed(res, "Invalid payment method", 400);
    }

    const weight = getWeightCart(cart);

    const [courierCost] = await Promise.all([
      getCourierCost(courier.key, address.subdistrictId, weight * 1000),
    ]);

    if (!courierCost) {
      return response.failed(res, "Courier service not available", 400);
    }

    const shippingCost = parseInt(
      getCourierServiceByResponse(courierCost, courierService)
    );

    const total = getTotalCart(cart) + shippingCost;

    const updateProductStock = cart.map((item: any) => {
      return prisma.products.update({
        where: {
          id: item.productId,
        },
        data: {
          stock: {
            decrement: item.quantity,
          },
        },
      });
    });

    const getPayment = await createPayment({
      service: selectedPayment.id,
      amount: total,
      phone: address.phone,
      return_url: "http://localhost:3000",
    });

    const transaction = await prisma.$transaction([
      ...updateProductStock,

      prisma.transaction.create({
        data: {
          userId: req.userId,
          addressId: String(addressId),
          courierId: String(courierId),
          total: parseInt(total.toFixed(0)),
          shippingCost: shippingCost,
          totalWeight: parseFloat(weight.toFixed(2)),
          totalProduct: cart.length,
          payment: {
            create: {
              id: getPayment.unique_code,
              paymentType: String(selectedPayment.type),
              paymentMethod: String(selectedPayment.name),
              status: "Pending",
              amount: parseInt(total.toFixed(0)),
              expiredAt: new Date(getPayment.expired).toISOString(),
              checkoutUrl: getPayment?.checkout_url,
              qrCodeUrl: getPayment?.qrcode_url,
              qrContent: getPayment?.qr_content,
              virtualAccount: getPayment?.virtual_account,
              payment_code: getPayment?.payment_code,
            },
          },
          details: {
            create: cart.map((item: any) => ({
              productId: item.productId,
              quantity: item.quantity,
              price: item.product.price,
            })),
          },
        },
      }),

      prisma.cart.deleteMany({
        where: {
          userId: req.userId,
        },
      }),
    ]);

    whatsappNotificationProducer(
      address.phone,
      `Pesanan anda di Book-store telah diterima, harap segera melakukan pembayaran 
menggunakan ${selectedPayment.name} dengan total pembayaran Rp. ${total}

Pembayaran dapat dilakukan melalui link
${getPayment?.checkout_url} 
sebelum ${getPayment?.expired}`
    );

    return response.success(res, "Order placed successfully", {
      ...transaction[1],
      payment: getPayment,
    });
  } catch (error: any) {
    return response.failed(res, error.message, 500);
  }
};
