import { Request, Response } from "express";
import prisma from "../database/prisma.database";
import response from "../utils/response.util";
import { getCartByUserId, getWeightCart } from "../services/cart.service";
import { getAllCouriers, getCourierCost } from "../services/shipping.service";
import { getAddressByAddressUserId } from "../services/address.service";

export const getCouriers = async (req: Request, res: Response) => {
  try {
    const couriers = await prisma.courier.findMany();

    return response.success(res, "Couriers retrieved successfully", couriers);
  } catch (error: any) {
    return response.failed(res, "Error retrieving couriers", 500);
  }
};

export const getShippingCost = async (req: Request, res: Response) => {
  const { addressId } = req.query;

  try {
    const [cart, address, courier] = await Promise.all([
      getCartByUserId(req.userId),
      getAddressByAddressUserId(req.userId, addressId as string),
      getAllCouriers(),
    ]);

    if (!address || !cart || cart.length === 0) {
      return response.failed(res, "Invalid request", 400);
    }

    const ongkir = await Promise.all(
      courier.map(async (courier: any) => {
        const cost = await getCourierCost(
          courier.key,
          address.subdistrictId,
          getWeightCart(cart) * 1000
        );

        return {
          id: courier.id,
          courier: courier.name,
          image: courier.image,
          cost: cost,
        };
      })
    );

    return response.success(res, "Shipping cost retrieved successfully", {
      address,
      weight: getWeightCart(cart),
      ongkir,
    });
  } catch (error: any) {
    return response.failed(res, "Error retrieving shipping cost", 500);
  }
};
