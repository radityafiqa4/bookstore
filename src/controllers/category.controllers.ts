import { Request, Response } from "express";
import response from "../utils/response.util";
import prisma from "../database/prisma.database";
import dotenv from "dotenv";
import stringToSlug from "../utils/string/stringToSlug";
dotenv.config();

export const createCategory = async (req: Request, res: Response) => {
  let { name } = req.body;

  try {
    const category = await prisma.categories.create({
      data: {
        name,
        slug: stringToSlug(name),
      },
    });

    return response.success(res, "Category created", category);
  } catch (error: any) {
    return response.failed(res, "Failed to create category", 500);
  }
};

export const getCategories = async (req: Request, res: Response) => {
  try {
    const categories = await prisma.categories.findMany();

    return response.success(res, "Success get categories", categories);
  } catch (error: any) {
    return response.failed(res, "Failed to get categories", 500);
  }
};

export const getCategory = async (req: Request, res: Response) => {
  let { slug } = req.params;

  try {
    const category = await prisma.categories.findFirst({
      where: { OR: [{ id: slug }, { slug }] },
    });

    if (!category) {
      return response.failed(res, "Category not found", 404);
    }

    return response.success(res, "Success get category", category);
  } catch (error: any) {
    return response.failed(res, "Failed to get category", 500);
  }
};

export const updateCategory = async (req: Request, res: Response) => {
  let { id } = req.params;
  let { name } = req.body;

  try {
    const category = await prisma.categories.update({
      where: { id: id },
      data: {
        name,
        slug: stringToSlug(name),
      },
    });

    return response.success(res, "Category updated", category);
  } catch (error: any) {
    return response.failed(res, "Failed to update category", 500);
  }
};

export const deleteCategory = async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const category = await prisma.categories.delete({
      where: { id: id },
    });

    return response.success(res, "Category deleted", category);
  } catch (error: any) {
    return response.failed(res, "Failed to delete category", 500);
  }
};
