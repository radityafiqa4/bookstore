import { Request, Response } from "express";
import response from "../utils/response.util";
import prisma from "../database/prisma.database";

export const addToCart = async (req: Request, res: Response) => {
  const { productId, quantity } = req.body;
  const userId = req.userId;

  try {
    // Find the product by ID
    const product = await prisma.products.findUnique({
      where: { id: productId },
    });

    if (!product) {
      await prisma.cart.deleteMany({
        where: {
          userId,
          productId,
        },
      });

      return response.failed(res, "Product not found", 404);
    }

    // Find the cart item by user ID and product ID
    let cartItem = await prisma.cart.findFirst({
      where: { userId, productId },
    });

    if (cartItem) {
      // If the cart item exists, update the quantity
      cartItem = await prisma.cart.update({
        where: { id: cartItem.id },
        data: {
          quantity: product.stock < quantity ? product.stock : quantity,
        },
      });
    } else {
      // If the cart item doesn't exist, create a new one
      cartItem = await prisma.cart.create({
        data: {
          userId,
          productId,
          quantity: product.stock < quantity ? product.stock : quantity,
        },
      });
    }

    return response.success(res, "Product added to cart", cartItem);
  } catch (error: any) {
    return response.failed(res, "Failed to add product to cart", 500);
  }
};

export const getCart = async (req: Request, res: Response) => {
  try {
    const cart = await prisma.cart.findMany({
      where: { userId: req.userId },
      include: {
        product: true,
      },
    });

    return response.success(res, "Success get cart", cart);
  } catch (error: any) {
    return response.failed(res, "Failed to get cart", 500);
  }
};

export const editCart = async (req: Request, res: Response) => {
  let { quantity, productId } = req.body;

  try {
    const product = await prisma.products.findUnique({
      where: { id: productId },
    });

    if (!product) {
      return response.failed(res, "Product not found", 404);
    }

    if (quantity <= 0) {
      await prisma.cart.deleteMany({
        where: {
          userId: req.userId,
          productId,
        },
      });
    } else {
      await prisma.cart.updateMany({
        where: {
          userId: req.userId,
          productId,
        },
        data: {
          quantity,
        },
      });
    }

    return response.success(res, "Cart updated", null);
  } catch (error: any) {
    return response.failed(res, "Failed to update cart", 500);
  }
};
