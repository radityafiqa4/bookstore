import { Request, Response } from "express";
import response from "../utils/response.util";
import prisma from "../database/prisma.database";
import dotenv from "dotenv";
dotenv.config();

interface UserRequest extends Request {
  userId: string;
}

export const createUserAddress = async (req: UserRequest, res: Response) => {
  let {
    name,
    street,
    provinceId,
    districtId,
    subdistrictId,
    postalCode,
    phone,
  } = req.body;

  const userId = req.userId;

  try {
    const newAddress = await prisma.address.create({
      data: {
        name,
        street,
        provinceId,
        districtId,
        subdistrictId,
        postalCode: String(postalCode),
        phone,
        userId,
      },
    });
    return response.success(res, "Address created", newAddress);
  } catch (error: any) {
    return response.failed(res, "Failed to create address", 500);
  }
};

export const getUserAddresses = async (req: UserRequest, res: Response) => {
  const userId = req.userId;
  try {
    const addresses = await prisma.address.findMany({
      where: { userId: String(userId) },
      include: {
        province: true,
        district: true,
        subdistrict: true,
      },
    });

    return response.success(res, "Success get addresses", addresses);
  } catch (error: any) {
    return response.failed(res, "Failed to get addresses", 500);
  }
};

export const updateUserAddress = async (req: Request, res: Response) => {
  let { id } = req.params;
  let {
    name,
    street,
    provinceId,
    districtId,
    subdistrictId,
    postalCode,
    phone,
  } = req.body;

  try {
    const address = await prisma.address.update({
      where: { id: id },
      data: {
        name,
        street,
        provinceId,
        districtId,
        subdistrictId,
        postalCode: String(postalCode),
        phone,
      },
    });

    return response.success(res, "Address updated", address);
  } catch (error: any) {
    return response.failed(res, "Failed to update address", 500);
  }
};

export const deleteUserAddress = async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const address = await prisma.address.delete({
      where: { id: id },
    });

    return response.success(res, "Address deleted", address);
  } catch (error: any) {
    return response.failed(res, "Failed to delete address", 500);
  }
};

export const getAddress = async (req: Request, res: Response) => {
  const { provinceId, districtId, subdistrictId } = req.query;

  try {
    let address: any;

    if (subdistrictId) {
      address = await prisma.subdistrict.findFirst({
        where: {
          id: Number(subdistrictId),
        },
      });
    }
    if (districtId) {
      address = await prisma.subdistrict.findMany({
        where: {
          districtId: Number(districtId),
        },
      });
    }
    if (provinceId) {
      address = await prisma.district.findMany({
        where: {
          provinceId: Number(provinceId),
        },
      });
    }

    address = await prisma.province.findMany();

    return response.success(res, "Success get address", address);
  } catch (error: any) {
    return response.failed(res, error.message, 500);
  }
};
