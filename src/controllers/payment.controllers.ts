import { Request, Response } from "express";
import response from "../utils/response.util";
import prisma from "../database/prisma.database";
import { whatsappNotificationProducer } from "../producers/whatsapp.producers";

export const paymentCallback = async (req: Request, res: Response) => {
  let { pay_id, unique_code, status } = req.body;
  try {
    const updatePayment = await prisma.payment.update({
      include: {
        transaction: {
          include: {
            address: true,
          },
        },
      },
      where: { id: unique_code },
      data: {
        transactionRef: pay_id,
        status,
        transaction: {
          update: {
            status: status === "Success" ? "PAID" : "CANCEL",
          },
        },
      },
    });

    if (status !== "Success") {
      const transaction = await prisma.transactionDetail.findMany({
        where: {
          transactionId: updatePayment.transactionId,
        },
      });

      const productUpdate = transaction.map((detail: any) => {
        return prisma.products.update({
          where: {
            id: detail.productId,
          },
          data: {
            stock: {
              increment: detail.quantity,
            },
          },
        });
      });

      await prisma.$transaction(productUpdate);
    }

    const { address } = updatePayment.transaction;

    if (status === "Success") {
      whatsappNotificationProducer(
        address.phone,
        `Terima kasih telah melakukan pembayaran, pesanan anda diBookstore dengan nomor order ${updatePayment.transaction.id} 
sejumlah ${updatePayment.transaction.total}
telah kami terima dan sedang kami proses.`
      );
    } else {
      whatsappNotificationProducer(
        address.phone,
        `Maaf, pesanan anda di Bookstore dengan nomor order ${updatePayment.transaction.id} telah dibatalkan. 
karena pembayaran tidak berhasil.
Silahkan pesan kembali jika masih berminat.`
      );
    }

    return response.success(res, "Payment processed successfully", {
      pay_id,
      unique_code,
      status,
    });
  } catch (error: any) {
    return response.failed(res, "Failed to process payment", 500);
  }
};
