import { Request, Response } from "express";
import response from "../utils/response.util";
import prisma from "../database/prisma.database";
import dotenv from "dotenv";
import stringToSlug from "../utils/string/stringToSlug";
import cloudinary from "../utils/cloudinary.utils";
import {
  getProductReviewAvg,
  getProductTotalSold,
} from "../services/product.service";
dotenv.config();

export const createProduct = async (req: Request, res: Response) => {
  let { ...rest } = req.body;

  try {
    if (req.images.length === 0) {
      return response.failed(res, "Please upload at least 1 image", 400);
    }

    if (rest.thumbnail >= req.images.length) {
      return response.failed(res, "Thumbnail index not found", 400);
    }

    const randomNumber = Math.floor(Math.random() * 100) + 1;

    const product = await prisma.products.create({
      data: {
        name: rest.name,
        slug: `${stringToSlug(rest.name)}-${randomNumber}`,
        price: parseInt(rest.price),
        publishedAt: rest.publishedAt,
        publisher: rest.publisher,
        description: rest.description,
        thumbnail: req.images[rest.thumbnail].secure_url,
        width: parseInt(rest.width),
        height: parseInt(rest.height),
        pages: parseInt(rest.pages),
        language: rest.language,
        isbn: rest.isbn,
        weight: parseFloat(rest.weight),
        sku: rest.sku,
        stock: parseInt(rest.stock),
        images: {
          create: req.images.map((image: any) => ({
            id: image?.asset_id,
            url: image?.secure_url,
          })),
        },
      },
    });

    const createCategories = async () => {
      await prisma.productsCategories.createMany({
        data: rest.categories.map((categoryId: number) => ({
          productId: product.id,
          categoryId,
        })),
      });
    };

    const createAuthors = async () => {
      await prisma.authorsProducts.createMany({
        data: rest.authors.map((authorId: number) => ({
          productId: product.id,
          authorId,
        })),
      });
    };

    await Promise.all([createCategories(), createAuthors()]);

    return response.success(res, "Product created", product);
  } catch (error: any) {
    return response.failed(res, "Failed to create product", 500);
  }
};

export const updateProduct = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { ...rest } = req.body;

  try {
    const product = await prisma.products.findUnique({
      include: {
        images: true,
      },
      where: { id },
    });

    if (!product) {
      return response.failed(res, "Product not found", 404);
    }

    const updatedData: any = {
      name: rest.name ?? product.name,
      price: parseInt(rest.price) ?? product.price,
      publishedAt: rest.publishedAt ?? product.publishedAt,
      publisher: rest.publisher ?? product.publisher,
      description: rest.description ?? product.description,
      thumbnail: product.images[rest.thumbnail]?.url ?? product.thumbnail,
      width: parseInt(rest.width) ?? product.width,
      height: parseInt(rest.height) ?? product.height,
      pages: parseInt(rest.pages) ?? product.pages,
      language: rest.language ?? product.language,
      isbn: rest.isbn ?? product.isbn,
      weight: parseFloat(rest.weight) ?? product.weight,
      sku: rest.sku ?? product.sku,
      stock: parseInt(rest.stock) ?? product.stock,
    };

    const newProduct = await prisma.products.update({
      where: { id },
      data: updatedData,
    });

    const updateCategories = async () => {
      if (rest.categories) {
        await prisma.productsCategories.deleteMany({
          where: {
            productId: id,
          },
        });

        await prisma.productsCategories.createMany({
          data: rest.categories.map((categoryId: number) => ({
            productId: id,
            categoryId,
          })),
        });
      }
    };

    const updateAuthors = async () => {
      if (rest.authors) {
        await prisma.authorsProducts.deleteMany({
          where: {
            productId: id,
          },
        });

        await prisma.authorsProducts.createMany({
          data: rest.authors.map((authorId: number) => ({
            productId: id,
            authorId,
          })),
        });
      }
    };

    await Promise.all([updateCategories(), updateAuthors()]);

    return response.success(res, "Product updated", newProduct);
  } catch (error: any) {
    return response.failed(res, error.message, 500);
  }
};

export const getProducts = async (req: Request, res: Response) => {
  try {
    let { page, limit, query, category, author } = req.query;

    // Parse query parameters
    const pageNumber = Math.max(1, parseInt(page as string) || 1);
    const limitNumber = Math.max(
      1,
      Math.min(50, parseInt(limit as string) || 20)
    );

    // Construct filter object
    const queryFilter = query ? { name: { contains: query as string } } : {};
    const categoryFilter = category
      ? { categories: { some: { category: { slug: category as string } } } }
      : {};
    const authorFilter = author
      ? {
          authors: {
            some: { author: { name: { contains: author as string } } },
          },
        }
      : {};

    // Construct filter object
    const filter = {
      where: {
        ...queryFilter,
        ...categoryFilter,
        ...authorFilter,
      },
    };

    // Fetch products based on filter and pagination
    const products = await prisma.products.findMany({
      skip: (pageNumber - 1) * limitNumber,
      take: limitNumber,
      ...filter,
    });

    const productsId = products.map((product: any) => product.id);

    const [totalSoldProduct, reviewProduct] = await Promise.all([
      getProductTotalSold(productsId),
      getProductReviewAvg(productsId),
    ]);

    // Map totalSold to new map
    const productsDetail = products.map((product: any) => {
      const totalSold = totalSoldProduct.find(
        (item: any) => item.productId === product.id
      );

      const review = reviewProduct.find(
        (item: any) => item.productId === product.id
      );

      return {
        ...product,
        total_sold: totalSold?._count?.id || 0,
        rating: review?._avg?.rating || 0,
      };
    });

    // Count total data matching the filter
    const totalData = await prisma.products.count(filter);

    // Return response with pagination metadata
    return response.success(res, "Success get products", productsDetail, {
      current_page: pageNumber,
      limit: limitNumber,
      total_data: totalData,
      total_page: Math.ceil(totalData / limitNumber),
    });
  } catch (error: any) {
    return response.failed(res, "Failed to get products", 500);
  }
};

export const getProduct = async (req: Request, res: Response) => {
  let { slug } = req.params;

  try {
    const product = await prisma.products.findFirst({
      where: { OR: [{ id: slug }, { slug }] },
      include: {
        categories: {
          select: {
            category: true,
          },
        },
        authors: {
          select: {
            author: true,
          },
        },
        images: true,
      },
    });

    if (!product) {
      return response.failed(res, "Product not found", 404);
    }

    const [totalSoldProduct, reviewProduct] = await Promise.all([
      getProductTotalSold([product.id]),
      getProductReviewAvg([product.id]),
    ]);

    return response.success(res, "Success get product", {
      ...product,
      total_sold: totalSoldProduct[0]?._count?.id || 0,
      rating: reviewProduct[0]?._avg?.rating || 0,
    });
  } catch (error: any) {
    return response.failed(res, "Failed to get product", 500);
  }
};

export const deleteProduct = async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const product = await prisma.products.delete({
      where: { id },
    });

    return response.success(res, "Product deleted", product);
  } catch (error: any) {
    return response.failed(res, "Failed to delete product", 500);
  }
};

export const addImageProduct = async (req: Request, res: Response) => {
  let { id } = req.params;

  try {
    const product = await prisma.products.findUnique({
      where: { id },
    });

    if (!product) {
      return response.failed(res, "Product not found", 404);
    }

    if (req.images.length === 0) {
      return response.failed(res, "Please upload at least 1 image", 400);
    }

    await prisma.images.createMany({
      data: req.images.map((image: any) => ({
        productId: id,
        id: image?.asset_id,
        url: image?.secure_url,
      })),
    });

    return response.success(
      res,
      "Image added",
      req.images.map((image: any) => ({
        id: image?.asset_id,
        url: image?.secure_url,
      }))
    );
  } catch (error: any) {
    return response.failed(res, "Failed to add image", 500);
  }
};

export const deleteImageProduct = async (req: Request, res: Response) => {
  let { id, imageId } = req.params;

  try {
    const image = await prisma.images.findUnique({
      where: { id: imageId },
    });

    if (!image) {
      return response.failed(res, "Image not found", 404);
    }

    await prisma.images.delete({
      where: { id: imageId },
    });

    if (image.url.startsWith("https://res.cloudinary.com/")) {
      await cloudinary.v2.uploader.destroy(image.id);
    }

    return response.success(res, "Image deleted", image);
  } catch (error: any) {
    return response.failed(res, "Failed to delete image", 500);
  }
};
