import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Request, Response } from "express";
import response from "../utils/response.util";
import UserMapper from "../utils/data/mapping/user.util";
import prisma from "../database/prisma.database";
import dotenv from "dotenv";
import moment from "moment";
import { whatsappNotificationProducer } from "../producers/whatsapp.producers";
import { randomString } from "../utils/string/random.utils";
dotenv.config();

export const login = async (req: Request, res: Response) => {
  let { email, password } = req.body;

  try {
    // Check email
    const user = await prisma.user.findUnique({
      where: { email },
    });

    if (!user) {
      return response.failed(res, "Email/Password is incorrect", 400);
    }

    // Check password
    const validPassword = await bcrypt.compare(password, user.password);

    if (!validPassword) {
      return response.failed(res, "Email/Password is incorrect", 400);
    }

    // Generate token
    const token = jwt.sign(
      { id: user.id, role: user.role },
      process.env.JWT_SECRET || "secret",
      {
        expiresIn: "1d",
      }
    );

    // Response
    return response.success(res, "Login success", {
      ...UserMapper(user),
      token,
    });
  } catch (error: any) {
    return response.failed(res, "Failed to login", 500);
  }
};

export const register = async (req: Request, res: Response) => {
  let { name, email, password } = req.body;

  try {
    // Hash password
    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    password = await bcrypt.hash(password, salt);

    // Create user
    const user = await prisma.user.create({
      data: {
        name,
        email,
        password,
      },
    });

    if (!user) {
      return response.failed(res, "Failed to create user", 500);
    }

    // Response
    return response.success(res, "Register success", UserMapper(user));
  } catch (error: any) {
    return response.failed(res, "Failed to register", 500);
  }
};

export const verifyPhone = async (req: Request, res: Response) => {
  const userId = req.userId;
  const { phone } = req.body;

  try {
    const user = await prisma.user.findUnique({
      where: { id: userId, is_verified: false },
    });

    if (!user) {
      return response.failed(res, "User not found or already verified", 400);
    }

    if (moment().diff(user.updatedAt, "minutes") < 1) {
      return response.failed(res, "Please wait 1 minute to resend code", 400);
    }

    const code = randomString("numeric", 6);

    await prisma.user.update({
      where: { id: userId },
      data: {
        phone,
        code,
      },
    });

    whatsappNotificationProducer(
      phone,
      `Your book-store verification code is ${code}`
    );

    return response.success(res, "Verification sent", null);
  } catch (error: any) {
    return response.failed(res, "Failed to send verification", 500);
  }
};

export const verifyPhoneCode = async (req: Request, res: Response) => {
  const userId = req.userId;
  const { code } = req.body;

  try {
    const user = await prisma.user.findUnique({
      where: { id: userId, is_verified: false },
    });

    if (!user) {
      return response.failed(res, "User not found or already verified", 400);
    }

    // check if code is not expired (5 minutes)
    if (moment().diff(user.updatedAt, "minutes") > 5 || user.code !== code) {
      return response.failed(res, "Code is invalid or expired", 400);
    }

    await prisma.user.update({
      where: { id: userId },
      data: {
        is_verified: true,
      },
    });

    return response.success(res, "Phone verified", null);
  } catch (error: any) {
    return response.failed(res, "Failed to verify phone", 500);
  }
};
