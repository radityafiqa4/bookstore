import { Request, Response } from "express";

import response from "../utils/response.util";
import prisma from "../database/prisma.database";

export const getReviewsByTransactionId = async (
  req: Request,
  res: Response
) => {
  const { transactionId } = req.params;

  try {
    const transaction = await prisma.transaction.findUnique({
      where: {
        id: transactionId,
        userId: req.userId,
      },
      include: {
        details: {
          include: {
            product: true,
          },
        },
      },
    });

    if (!transaction) {
      return response.failed(res, "Transaction not found", 404);
    }

    if (transaction.status !== "COMPLETED") {
      return response.failed(res, "Transaction not yet completed", 400);
    }

    const reviews = await prisma.review.findMany({
      include: {
        product: true,
      },
      where: {
        transactionId,
      },
    });

    const products = transaction.details.map((detail: any) => detail.product);
    const unreviewedProducts = products.filter(
      (product: any) =>
        !reviews.find((review: any) => review.productId === product.id)
    );

    return response.success(res, "Reviews retrieved successfully", {
      unreviewedProducts,
      reviews,
    });
  } catch (error: any) {
    return response.failed(res, "Error retrieving reviews", 500);
  }
};

export const createReview = async (req: Request, res: Response) => {
  try {
    const { transactionId, productId } = req.params;
    const { rating, review } = req.body;

    // Check if transaction exist
    const transaction = await prisma.transaction.findFirst({
      where: {
        id: transactionId,
        userId: req.userId,
        status: "COMPLETED",
        review: {
          some: {
            productId,
          },
        },
      },
    });

    if (transaction) {
      return response.failed(res, "You already reviewed this product", 400);
    }

    const reviewCreated = await prisma.review.create({
      data: {
        transactionId,
        productId,
        userId: req.userId,
        rating,
        comment: review,
      },
    });

    return response.success(res, "Review created successfully", reviewCreated);
  } catch (error: any) {
    console.log(error);
    return response.failed(res, "Error creating review", 500);
  }
};
