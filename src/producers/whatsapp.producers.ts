import { connect } from "../utils/rabbitmq.utils";
import CONSTS from "../utils/consts.utils";

export const whatsappNotificationProducer = async (
  phone: string,
  message: string
) => {
  try {
    const { connection, channel } = await connect();

    await channel.assertQueue(CONSTS.whatsappNotification, { durable: true });
    channel.sendToQueue(
      CONSTS.whatsappNotification,
      Buffer.from(JSON.stringify({ phone, message })),
      {
        persistent: true,
      }
    );

    setTimeout(() => {
      connection.close();
    }, 500);
  } catch (error) {
    console.log(error);
  }
};
