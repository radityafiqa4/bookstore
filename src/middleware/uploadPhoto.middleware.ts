import multer from "multer";
import path from "path";
import { NextFunction, Response, Request } from "express";
import response from "../utils/response.util";
import cloudinary from "../utils/cloudinary.utils";

declare global {
  namespace Express {
    interface Request {
      images?: any;
    }
  }
}

const multerUpload = multer({
  storage: multer.memoryStorage(),
  fileFilter: (_, file, cb) => {
    const ext = path.extname(file.originalname);
    if ([".jpg", ".png", ".jpeg", ".webp"].includes(ext.toLowerCase())) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  },
  limits: {
    files: 5,
    fileSize: 1024 * 1024 * 3, // 3MB file size limit
  },
});

const upload = (req: Request, res: Response, next: NextFunction) => {
  const multerSingle = multerUpload.array("images", 5);
  multerSingle(req, res, async (err: any) => {
    if (err instanceof multer.MulterError) {
      return response.failed(res, err.message, 400);
    } else if (err) {
      return response.failed(res, err.message, 400);
    }

    if (req.files) {
      try {
        const promises = (req.files as Express.Multer.File[]).map((file) => {
          return new Promise((resolve, reject) => {
            cloudinary.v2.uploader
              .upload_stream({ resource_type: "auto" }, (error, result) => {
                if (error) {
                  reject(error);
                } else {
                  resolve(result);
                }
              })
              .end(file.buffer);
          });
        });

        const uploadedFiles = await Promise.all(promises);
        req.images = uploadedFiles;
        next();
      } catch (error) {
        return response.failed(
          res,
          "Failed to upload files to Cloudinary",
          500
        );
      }
    } else {
      next();
    }
  });
};

export default upload;
