import { sendWhatsappMessage } from "../services/whatsapp.service";
import CONSTS from "../utils/consts.utils";
import { connect } from "../utils/rabbitmq.utils";

export const whatsappNotificationConsumer = async () => {
  const { connection, channel } = await connect();

  await channel.assertQueue(CONSTS.whatsappNotification, { durable: true });

  console.log("Waiting for messages in %s", CONSTS.whatsappNotification);

  channel.consume(CONSTS.whatsappNotification, async (msg) => {
    if (msg) {
      const { phone, message } = JSON.parse(msg.content.toString());

      console.log(`[${CONSTS.whatsappNotification}] Received: %s`, phone);

      const sendMessages = await sendWhatsappMessage(phone, message);

      if (sendMessages.status === true) {
        console.log(
          `[${CONSTS.whatsappNotification}] Success send message to %s`,
          phone
        );
        channel.ack(msg);
      } else {
        console.log(
          `[${CONSTS.whatsappNotification}] Failed send message to %s`,
          phone
        );
      }
    }
  });
};
